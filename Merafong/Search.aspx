﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Search.aspx.vb" Inherits="Merafong.Search" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" Skin="Silk" />
<telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <div class="jumbotron vertical-center">
        <h2 style="text-align:center;">Search for Property information using the GIS platform.</h2>
        <hr />
        <div style="text-align:center;" id="erf" runat="server">
            <h4 style="text-align:center;">Erf Search Tool</h4>
            <br />            
                <asp:Label style="line-height:25px;" Height="25" runat="server"  Text="Erf Number:" Font-Bold="true"></asp:Label>
                <asp:TextBox ID="txtErf" style="line-height:30px;" height="30" runat="server" ></asp:TextBox>
                <telerik:RadButton style="line-height:30px;" Height="30" ID="btnErfSearch" runat="server" Text="Search" Skin="Silk" ></telerik:RadButton>
            <telerik:RadGrid ClientSettings-EnableRowHoverStyle="true" Visible="false" ID="searchGrid" runat="server" CellSpacing="-1" DataSourceID="erfSQL" GridLines="Both" GroupPanelPosition="Top" Skin="Silk">
                <MasterTableView AutoGenerateColumns="False" DataSourceID="erfSQL">
                    <Columns>
                        <telerik:GridBoundColumn DataField="Sg_No" FilterControlAltText="Filter Sg_No column" HeaderText="SG No" SortExpression="Sg_No" UniqueName="Sg_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Stand_No" DataType="System.Decimal" FilterControlAltText="Filter Stand_No column" HeaderText="Stand No" SortExpression="Stand_No" UniqueName="Stand_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Ward" DataType="System.Decimal" FilterControlAltText="Filter Ward column" HeaderText="Ward" SortExpression="Ward" UniqueName="Ward">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SGTWNEXT" FilterControlAltText="Filter SGTWNEXT column" HeaderText="Town Ext" SortExpression="SGTWNEXT" UniqueName="SGTWNEXT">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn HeaderText="Regulations" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Regulations" CommandName="Regulations" ButtonType="ImageButton" ImageUrl="Images/spatial.jpg" ></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn HeaderText="Accounts" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Accounts" CommandName="Accounts" ButtonType="ImageButton" ImageUrl="Images/spatial.jpg" ></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn HeaderText="Services" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Services" CommandName="Services" ButtonType="ImageButton" ImageUrl="Images/spatial.jpg" ></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="erfSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Merafong_sde %>" SelectCommand="select a.Sg_No, a.Stand_No,  a.Ward, SGTWNEXT from Merafong_sde.sde.PROPERTIES a
                            inner join Merafong_sde.sde.URBANRURALCADASTRE b
                            on a.Sg_No = b.SGCODE where a.Stand_No = @Stand_No">
            <SelectParameters>
                <asp:sessionparameter name="Stand_No" sessionfield="Stand_No" type="String" />
            </SelectParameters>
            </asp:SqlDataSource>
        </div>         
        <div style="text-align:center;" id="street" runat="server">
            <h4 style="text-align:center;">Street Address Search Tool</h4>
            <br />
            <asp:Label runat="server" Text="Street Number: " Font-Bold="true"></asp:Label>&nbsp
            <asp:TextBox runat="server" Width="50" Height="30"></asp:TextBox>
            &nbsp<asp:Label runat="server" Text=" and Street Name: " Font-Bold="true"></asp:Label>&nbsp
            <asp:TextBox Height="30" runat="server" Width="350"></asp:TextBox>
            <telerik:RadButton Height="30" ID="RadButton1" runat="server" Text="Search" Skin="Silk"></telerik:RadButton>
        </div>
        <div style="text-align:center;" id="count" runat="server">
             <h4 style="text-align:center;">Count Erven Tool</h4>
            <br />
            <asp:Label runat="server" Text="Select Town Extenstion:" Font-Bold="true"></asp:Label>
            <telerik:RadDropDownList ID="RadDropDownList1" DefaultMessage="Please Select a Town" runat="server" DataSourceID="Merafong_SDE" DataTextField="TSHP_NAME" DataValueField="TSHP_NAME" Skin="Silk" Width="250px"></telerik:RadDropDownList>
             
            <telerik:RadButton Height="30" ID="RadButton2" runat="server" Text="Count" Skin="Silk"></telerik:RadButton>
        </div>
        <div style="text-align:center;" id="age" runat="server">
            <h4 style="text-align:center;">Accounts: Age Analysis Maps per Ward Boundary</h4>
            <br />
            <asp:Label runat="server" Text="Select Ward: " Font-Bold="true"></asp:Label>&nbsp
             <telerik:RadDropDownList ID="RadDropDownList2" DefaultMessage="Please Select a Ward" runat="server" DataSourceID="Merafong_SDE2" DataTextField="WARD_NO" DataValueField="WARD_NO" Skin="Silk" Width="160px"></telerik:RadDropDownList><br />
            <asp:Label runat="server" Text="Select Age Analysis Peroid:" Font-Bold="true"></asp:Label>
             <telerik:RadDropDownList ID="RadDropDownList3" runat="server" DefaultMessage="Select Age Peroid" Skin="Silk">
                 <Items>
                     <telerik:DropDownListItem runat="server" Text="Current" />
                     <telerik:DropDownListItem runat="server" Text="30 Days" />
                     <telerik:DropDownListItem runat="server" Text="60 Days" />
                     <telerik:DropDownListItem runat="server" Text="90 Days" />
                     <telerik:DropDownListItem runat="server" Text="120 Days" />
                 </Items>
                                 
             </telerik:RadDropDownList> <br />
            <telerik:RadButton Height="30" ID="RadButton3" runat="server" Text="Search" Skin="Silk"></telerik:RadButton>
        </div>
        <hr />
    </div>
</telerik:RadAjaxPanel>
    <asp:SqlDataSource ID="Merafong_SDE" runat="server" ConnectionString="<%$ ConnectionStrings:Merafong_sde %>" SelectCommand="SELECT DISTINCT [TSHP_NAME] FROM [URBANTOWNEXT]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Merafong_SDE2" runat="server" ConnectionString="<%$ ConnectionStrings:Merafong_sde %>" SelectCommand="SELECT DISTINCT [WARD_NO] FROM [WARDS2005]"></asp:SqlDataSource>
</asp:Content>
    