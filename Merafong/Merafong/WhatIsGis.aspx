﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="WhatIsGis.aspx.vb" Inherits="Merafong.WhatIsGis" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron vertical-center">
        <h2 style="text-align:center;">What is GIS?</h2>
        <p style="text-align:center;">GIS abbreviates the term "Geographic Information Systems"</p>
        <p style="text-align:center;">GIS answers the "WHERE?" question about information.
            For Example:
            Using a GIS system you not only know that there are 10 schools in a specific area, but you also know where they are located. 
        </p>
        <p style="text-align:center;">
            At the simplest level, GIS can be thought of as a high-tech equivalent of a map. However, not only can paper maps be produced far quicker and more efficiently, the storage of data in an easily accessible digital format enables easy access, complex analysis and modeling not previously possible. 
        </p>
        <p style="text-align:center;">
            The key word to this technology is Geography - this usually means that the data (or at least some proportion of the data) is spatial, in other words, data that is in some way referenced to locations on the earth. Coupled with this data is usually data known as "attribute" data. Attribute data generally defined as additional information, which can be tied to spatial data.
            Back to school example:
            The actual location of the schools is the spatial data. Additional data such as the school name, level of education taught, school capacity would make up the attribute data. It is the partnership of these two spatial data types that enable GIS to be such an effective problem solving tool. 
        </p>
        <p style="text-align:center;">
            Through this website we try to answer your "where?" questions!
        </p>
    </div>
</asp:Content>

