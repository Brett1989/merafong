﻿Public Class Hotlink
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim paths As String() = New String() {"~/Images/"}
        'This code sets RadFileExplorer's paths
        RadFileExplorer1.Configuration.ViewPaths = paths
        RadFileExplorer1.Configuration.UploadPaths = paths
        RadFileExplorer1.Configuration.DeletePaths = paths

        'Sets Max file size
        RadFileExplorer1.Configuration.MaxUploadFileSize = 10485760

        ' Enables Paging on the Grid
        RadFileExplorer1.AllowPaging = True
        ' Sets the page size
        RadFileExplorer1.PageSize = 20
    End Sub

End Class