﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class AccountsReport
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Merafong_sde").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lat, lng As String
        Dim query As String

        myConn.Open()
        query = "Select * from Merafong_sde.sde.PROPERTIES a inner join Merafong_sde.sde.URBANRURALCADASTRE b on a.Sg_No = b.SGCODE where a.Sg_No = '" & Session("Sg_No") & "'"
        cmd = New SqlCommand(Query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                lat = rdr.Item("Y")
                lng = rdr.Item("X")
                mapFrame.Src = "baseMap.html?lng=" & lng & "&lat=" & lat
            Loop
        End If
        myConn.Close()
    End Sub

End Class