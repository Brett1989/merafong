﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Zoning.aspx.vb" Inherits="Merafong.Zoning" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" Skin="Silk" />
<telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
</telerik:RadScriptBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">

    <div class="jumbotron">
        <h2 style="text-align:center;">Online Zoning Certificate</h2>
        <asp:Label style="line-height:25px;" Height="25" runat="server"  Text="Type an Erf Number:" Font-Bold="true"></asp:Label>
        <asp:TextBox ID="txtErf" style="line-height:30px;" height="30" runat="server" ></asp:TextBox>
        <telerik:RadButton style="line-height:30px;" Height="30" ID="btnErfSearch" runat="server" Text="Search" Skin="Silk" ></telerik:RadButton>
        <br />
        <p style="text-align:center">OR</p>
        <asp:Label runat="server" Text="Street Number: " Font-Bold="true"></asp:Label>&nbsp
        <asp:TextBox ID="txtStreetNo" runat="server"  style="line-height:30px;" height="30" Width="50"></asp:TextBox>
        &nbsp<asp:Label runat="server" Text=" and Street Name: " Font-Bold="true"></asp:Label>&nbsp
        <asp:TextBox ID="txtStreetName"  style="line-height:30px;" height="30" runat="server" Width="350"></asp:TextBox>
         <telerik:RadButton style="line-height:30px;" Height="30" ID="btnAddressSearch" runat="server" Text="Search" Skin="Silk"></telerik:RadButton>

        <telerik:RadGrid AllowPaging="true" PageSize="10" ClientSettings-EnableRowHoverStyle="true" Visible="False"  ID="erfGrid" runat="server" CellSpacing="-1" DataSourceID="erfSQL" GridLines="Both" GroupPanelPosition="Top" Skin="Silk">
                <MasterTableView AutoGenerateColumns="False" DataSourceID="erfSQL">
                   <Columns>
                        <telerik:GridBoundColumn DataField="Sg_No" FilterControlAltText="Filter Sg_No column" HeaderText="SG No" SortExpression="Sg_No" UniqueName="Sg_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Stand_No" DataType="System.Decimal" FilterControlAltText="Filter Stand_No column" HeaderText="Erf No" SortExpression="Erf No" UniqueName="Erf_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Portion" FilterControlAltText="Filter Portion column" HeaderText="Portion" SortExpression="Portion" UniqueName="Portion">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Remainder" FilterControlAltText="Filter Remainder column" HeaderText="Remainder" SortExpression="Remainder" UniqueName="Remainder">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SGTWNEXT" FilterControlAltText="Filter SGTWNEXT column" HeaderText="Town Ext" SortExpression="SGTWNEXT" UniqueName="SGTWNEXT">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Extension" FilterControlAltText="Filter Extension column" HeaderText="Ext" SortExpression="Extension" UniqueName="Extension">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Ward" DataType="System.Decimal" FilterControlAltText="Filter Ward column" HeaderText="Ward" SortExpression="Ward" UniqueName="Ward">
                        </telerik:GridBoundColumn>                        
                        <telerik:GridButtonColumn HeaderText="Zoning Certificate" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Zoning" CommandName="Zoning" ButtonType="ImageButton" ImageUrl="Images/spatial.jpg" ></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="erfSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Merafong_sde %>" >
            </asp:SqlDataSource>

        <telerik:RadGrid AllowPaging="true" PageSize="10" ClientSettings-EnableRowHoverStyle="true" ID="addressGrid" Visible="false" runat="server" DataSourceID="addressSQL" GroupPanelPosition="Top" Skin="Silk">
                 <MasterTableView AutoGenerateColumns="False" DataSourceID="addressSQL">
                    <Columns>
                        <telerik:GridBoundColumn DataField="Sg_No" FilterControlAltText="Filter Sg_No column" HeaderText="SG No" SortExpression="Sg_No" UniqueName="Sg_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Stand_No" DataType="System.Decimal" FilterControlAltText="Filter Stand_No column" HeaderText="Erf No" SortExpression="Erf No" UniqueName="Erf_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Portion" FilterControlAltText="Filter Portion column" HeaderText="Portion" SortExpression="Portion" UniqueName="Portion">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Remainder" FilterControlAltText="Filter Remainder column" HeaderText="Remainder" SortExpression="Remainder" UniqueName="Remainder">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SGTWNEXT" FilterControlAltText="Filter SGTWNEXT column" HeaderText="Town Ext" SortExpression="SGTWNEXT" UniqueName="SGTWNEXT">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Extension" FilterControlAltText="Filter Extension column" HeaderText="Ext" SortExpression="Extension" UniqueName="Extension">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Ward" DataType="System.Decimal" FilterControlAltText="Filter Ward column" HeaderText="Ward" SortExpression="Ward" UniqueName="Ward">
                        </telerik:GridBoundColumn>                        
                        <telerik:GridButtonColumn HeaderText="Zoning Certificate" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Zoning" CommandName="Zoning" ButtonType="ImageButton" ImageUrl="Images/spatial.jpg" ></telerik:GridButtonColumn>
                    </Columns>
                 </MasterTableView>
            </telerik:RadGrid>
           <asp:SqlDataSource ID="addressSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Merafong_sde %>" SelectCommand="select a.Sg_No, a.Stand_No, a.Portion, a.Remainder, b.SGTWNEXT, a.Extension, a.Ward from Merafong_sde.sde.PROPERTIES a
                            inner join Merafong_sde.sde.URBANRURALCADASTRE b
                            on a.Sg_No = b.SGCODE where (a.Street_Nam  LIKE '%' + @streetName + '%') and (a.Street_No LIKE '%' + @streetNo + '%')">
            <SelectParameters>
                <asp:sessionparameter name="streetName" sessionfield="streetName" type="String" />
                 <asp:sessionparameter name="streetNo" sessionfield="streetNo" type="String" />
            </SelectParameters>
            </asp:SqlDataSource>
    </div>

</telerik:RadAjaxPanel>

</asp:Content>
