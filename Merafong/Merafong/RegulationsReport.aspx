﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RegulationsReport.aspx.vb" Inherits="Merafong.RegulationsReport2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
     <style>
        td{
            padding-left: 5px;
        }
        th {
            color: black;
            background-color: gray;
            width: 200px;
            text-align: right;
        } 
        table, tr, th, td{
            border: 1px solid black;
            border-collapse: collapse;
             font-size: medium ;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="jumbotron">
          <h2 style="text-align:center;">Regulations Report</h2>
                <table  style="width:100%">
                    <tr>
                        <th>Stand Number:</th>
                        <td><%=Session("StandNumber") %></td>
                        <th>Portion:</th>
                        <td><%=Session("Portion") %></td>
                    </tr>
                    <tr>
                        <th>Street Name:</th>
                        <td><%=Session("StreetName") %></td>
                        <th>Street Number:</th>
                        <td><%=Session("StreetNo") %></td>
                    </tr>
                    <tr>
                        <th>SG Code:</th>
                        <td colspan="3"><%=Session("Sg_No") %></td>
                    </tr>
                     <tr>
                        <th>Town Extension:</th>
                        <td><%=Session("TownExt") %></td>
                        <th>Extention Number:</th>
                        <td><%=Session("Extension") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Property Size:</th>
                        <td colspan="3"><%=Session("PropertySize") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Purchase Amount:</th>
                        <td colspan="3"><%=Session("PurchaseAmount") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Property Valuation:</th>
                        <td colspan="3"><%=Session("PropertyValuation") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Improvement Valuation:</th>
                        <td colspan="3"><%=Session("Improvement") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Deed Number:</th>
                        <td colspan="3"><%=Session("DeedNumber") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Ward Number:</th>
                        <td colspan="3"><%=Session("Ward") %></td>
                    </tr>
                    <tr>
                         <th colspan="4" style="text-align:center">Property Regulations:</th>
                    </tr>
                    <tr>
                        <th colspan="1">Residential 1 (CV)(1)</th>
                        <td colspan="3"><%=Session("Reservoir") %></td>
                    </tr>              
                    <%If Session("addressSearch") = True Then%>     
                         <tr>
                            <th colspan="1">Street Boundary 5m:</th>
                            <td colspan="3"><%=Session("Reservoir1_2")%></td>
                        </tr>
                         <tr>
                            <th colspan="1">Rear Boundary 2m:</th>
                            <td colspan="3"><%=Session("Reservoir1_3") %></td>
                        </tr>
                        <tr>
                            <th colspan="1">Side Boundary 2m:</th>
                            <td colspan="3"><%=Session("Reservoir1_4") %></td>
                        </tr>
                        <tr>
                            <th colspan="1">Maximum 50% Coverage:</th>
                            <td colspan="3"><%=Session("Reservoir1_5") %></td>
                        </tr>            
                        <tr>
                            <th colspan="1">Maxmum 3 Storeys:</th>
                            <td colspan="3"><%=Session("Reservoir1_6") %></td>
                        </tr>      
                        <tr>
                            <th colspan="1">1.5% Floor Area Ratio:</th>
                            <td colspan="3"><%=Session("Reservoir1_7") %></td>
                        </tr>  
                    <%end if %>
                </table>
              <%If Session("addressSearch") = True Then%>     
             <div class="row">
                 <div class="col-md-6">
                    <div style="text-align:center;">
                        <iframe id="mapFrame" runat="server" height="400" width="400" ></iframe>
                    </div> 
                 </div>
                 <div class="col-md-6">
                      <div style="text-align:center;">
                        <img src="/Images/reg_legend.gif" />
                    </div>   
                 </div>
             </div>
             <%else %>
                    <div style="text-align:center;">
                        <img src="/Images/reg_legend.gif" />
                    </div>   
             <% End If %> 
        </div> 
</asp:Content>
