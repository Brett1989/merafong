﻿Imports System.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI

Public Class Zoning
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Merafong_sde").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnAddressSearch_Click(sender As Object, e As EventArgs) Handles btnAddressSearch.Click
        Dim streetNumber As String
        Dim streetname As String

        streetname = txtStreetName.Text
        streetNumber = txtStreetNo.Text

        Session("StreetName") = streetname
        Session("StreetNo") = streetNumber
        addressGrid.Visible = True
        erfGrid.Visible = False
    End Sub

    Private Sub btnErfSearch_Click(sender As Object, e As EventArgs) Handles btnErfSearch.Click
        Dim erf As String
        Dim pos As Integer

        erf = txtErf.Text
        pos = InStr(erf, "*")
        If pos < 2 Then
            erf = Right(erf, Len(erf) - pos)
        ElseIf pos > 2 Then
            erf = Left(erf, pos - 1)
        End If

        If pos > 0 Then
            erfSQL.SelectCommand = "SELECT a.Sg_No, a.Stand_No, a.Portion, a.Remainder, b.SGTWNEXT, a.Extension, a.Ward FROM PROPERTIES AS a INNER JOIN URBANRURALCADASTRE AS b ON a.Sg_No = b.SGCODE WHERE (a.Stand_No LIKE '%" & erf & "%')"
        Else
            erfSQL.SelectCommand = "SELECT a.Sg_No, a.Stand_No, a.Portion, a.Remainder, b.SGTWNEXT, a.Extension, a.Ward FROM PROPERTIES AS a INNER JOIN URBANRURALCADASTRE AS b ON a.Sg_No = b.SGCODE WHERE (a.Stand_No = " & erf & ")"
        End If

        Session("Stand_No") = erf
        erfGrid.Visible = True
        addressGrid.Visible = False
    End Sub


    Private Sub searchGrid_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles erfGrid.ItemCommand
        If e.CommandName = "Zoning" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(Session("Sg_No"))
        End If
        
    End Sub

    Private Sub addressGrid_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles addressGrid.ItemCommand
        If e.CommandName = "Zoning" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(Session("Sg_No"))
        End If
    End Sub

    Sub generateReport(sgcode As String)
        Dim query As String
        Session("Sg_No") = sgcode
        myConn.Open()
        query = "select a.Sg_No, a.Stand_No, a.Portion, a.Street_Nam, a.Street_No, b.SGTWNEXT, a.Extension, a.Property_S, a.Purchase_A, a.Property_V, a.Improvemen, a.Deed_No, a.Ward, c.Regulati_1  from Merafong_sde.sde.PROPERTIES a" & _
                    " inner join Merafong_sde.sde.URBANRURALCADASTRE b" & _
                    " on a.Sg_No = b.SGCODE" & _
                    " inner join Merafong_sde.sde.REGULATIONS c" & _
                    " on a.Sg_No = c.Sg_No" & _
                    " where a.Sg_No = '" & Session("Sg_No") & "'"
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                Session("StandNumber") = rdr.Item("Stand_No")
                Session("Portion") = rdr.Item("Portion")
                Session("StreetName") = rdr.Item("Street_Nam")
                Session("StreetNo") = rdr.Item("Street_No")
                Session("TownExt") = rdr.Item("SGTWNEXT")
                Session("Extension") = rdr.Item("Extension")
                'Session("PropertySize") = rdr.Item("Property_S")
                'Session("PurchaseAmount") = rdr.Item("Purchase_A")
                'Session("PropertyValuation") = rdr.Item("Property_V")
                ' Session("Improvement") = rdr.Item("Improvemen")
                'Session("DeedNumber") = rdr.Item("Deed_No")
                'Session("Ward") = rdr.Item("Ward")
                'Session("Reservoir") = rdr.Item("Regulati_1")
            Loop
        End If
        myConn.Close()

        'property regulations
        Dim i As Integer
        myConn.Open()
        query = "SELECT * FROM [Merafong_SDE].[sde].[REGULATIONS]  where Sg_No = '" & Session("Sg_No") & "'"
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        i = 1
        If rdr.HasRows Then
            Do While rdr.Read
                Select Case i
                    Case 1
                        Session("Reservoir") = rdr.Item("Regulati_1")
                    Case 2
                        Session("Reservoir1_2") = rdr.Item("Regulati_1")
                    Case 3
                        Session("Reservoir1_3") = rdr.Item("Regulati_1")
                    Case 4
                        Session("Reservoir1_4") = rdr.Item("Regulati_1")
                    Case 5
                        Session("Reservoir1_5") = rdr.Item("Regulati_1")
                End Select
                i = i + 1
            Loop
        End If
        myConn.Close()
        Response.Redirect("ZoningReport.aspx")
    End Sub

End Class