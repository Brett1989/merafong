﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TownPlanning.aspx.vb" Inherits="Merafong.TownPlanning" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
    <style>
        .rightAlign{
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <telerik:RadSkinManager ID="QsfSkinManager" runat="server" Skin="Silk" />
<telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
</telerik:RadScriptBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <div class="jumbotron">
        <h2 style="text-align:center;">Online Zoning Certificate</h2>
        <h4 style="text-align:center;">Login</h4>
        <h6 runat="server" id="lblLogin" style="color:red; text-align:center" visible="false" >Login Details Incorrect</h6>
        <asp:Label CssClass="rightAlign" runat="server" Width="400" Text="User Name:"></asp:Label>
        <asp:TextBox ID="txtUsername" runat="server" Width="400"></asp:TextBox>
        <br />
        <asp:Label CssClass="rightAlign" runat="server" Width="400" Text="Password:"></asp:Label>
        <asp:TextBox TextMode="Password" id="txtPassword" runat="server" Width="400"></asp:TextBox>
        <br />
        <div align="center">
            <telerik:RadButton ID="btnLogin" runat="server" Skin="Silk" Text="Login"></telerik:RadButton>
         </div>        
    </div>
</telerik:RadAjaxPanel>
</asp:Content>
