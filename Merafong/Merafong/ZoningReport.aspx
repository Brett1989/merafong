﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ZoningReport.aspx.vb" Inherits="Merafong.ZoningReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
     <style>
        td{
            padding-left: 5px;
        }
        th {
            color: black;
            background-color: gray;
            width: 200px;
            text-align: right;
        } 
        table, tr, th, td{
            border: 1px solid black;
            border-collapse: collapse;
             font-size: medium ;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
          <h2 style="text-align:center;">Online Zoning Certificate</h2>
                <table  style="width:100%">
                    <tr>
                        <th>Stand Number:</th>
                        <td><%=Session("StandNumber") %></td>
                        <th>Portion:</th>
                        <td><%=Session("Portion") %></td>
                    </tr>
                    <tr>
                        <th>Street Name:</th>
                        <td><%=Session("StreetName") %></td>
                        <th>Street Number:</th>
                        <td><%=Session("StreetNo") %></td>
                    </tr>
                    <tr>
                        <th>SG Code:</th>
                        <td colspan="3"><%=Session("Sg_No") %></td>
                    </tr>
                     <tr>
                        <th>Town Extension:</th>
                        <td><%=Session("TownExt") %></td>
                        <th>Extention Number:</th>
                        <td><%=Session("Extension") %></td>
                    </tr>
                      <tr>
                         <th colspan="4" style="text-align:center">Excerpt from the Conditions of Establishment for Annex_F(<%=Session("TownExt") %>):</th>
                    </tr>
                     <tr>
                        <th colspan="1">Use Zone:</th>
                        <td colspan="3"><%=Session("PurchaseAmount") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Notation as shown on the A-series Map:</th>
                        <td colspan="3"><iframe id="mapFrame" runat="server" height="400" width="400" ></iframe></td>
                    </tr>
                     <tr>
                        <th colspan="1">Permitted Uses:</th>
                        <td colspan="3"><%=Session("Improvement") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Uses permitted only with the consent of the responsible authority:</th>
                        <td colspan="3"><%=Session("DeedNumber") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Prohibited Uses:</th>
                        <td colspan="3"><%=Session("Ward") %></td>
                    </tr>
                    <tr>
                         <th colspan="4" style="text-align:center">Property Regulations:</th>
                    </tr>
                    <tr>
                        <th colspan="1">Residential 1 (CV)(1)</th>
                        <td colspan="3"><%=Session("Reservoir") %></td>
                    </tr>                 
                    <tr>
                            <th colspan="1">Rear Boundary 1m:</th>
                            <td colspan="3"><%=Session("Reservoir1_2")%></td>
                    </tr>
                    <tr>
                            <th colspan="1">Side Boundary 1m:</th>
                            <td colspan="3"><%=Session("Reservoir1_3") %></td>
                    </tr>
                    <tr>
                            <th colspan="1">Maximum 60% Coverage:</th>
                            <td colspan="3"><%=Session("Reservoir1_4")%></td>
                    </tr>            
                    <tr>
                            <th colspan="1">Maxmum 2 Storeys:</th>
                            <td colspan="3"><%=Session("Reservoir1_5")%></td>
                    </tr>      
                </table> 
    </div>       
</asp:Content>
