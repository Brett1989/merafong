﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports Telerik.Web.UI
Imports System.IO
Imports System.Drawing
Imports ClosedXML


Public Class Search
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Merafong_sde").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim page As String

        erf.Visible = False
        street.Visible = False
        count.Visible = False
        age.Visible = False

        page = Request.QueryString("page")
        Select Case page
            Case "erfNumber"
                erf.Visible = True
            Case "streetAddress"
                street.Visible = True
            Case "erven"
                count.Visible = True
            Case "ageAnalysis"
                age.Visible = True
        End Select

    End Sub

    Private Sub btnErfSearch_Click(sender As Object, e As EventArgs) Handles btnErfSearch.Click
        searchGrid.Visible = True
        Session("Stand_No") = txtErf.Text
    End Sub

    Private Sub btnAddressSearch_Click(sender As Object, e As EventArgs) Handles btnAddressSearch.Click
        addressGrid.Visible = True
        Session("streetNo") = txtStreetNo.Text
        Session("streetName") = txtStreetName.Text
    End Sub

    Private Sub btnErvenCount_Click(sender As Object, e As EventArgs) Handles btnErvenCount.Click
        Dim query As String
        Dim count As Integer

        erverHeader.Visible = True
        btnExport.Visible = True
        btnZoomTo.Visible = True
        count = 0
        query = "select * from Merafong_sde.sde.URBANRURALCADASTRE where SGTWNEXT = '" & dropErvenCount.SelectedValue & "'"
        myConn.Open()
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                count = count + 1
            Loop
        End If
        Session("ervenCount") = count
        Session("ervenSelected") = dropErvenCount.SelectedValue
        myConn.Close()

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        ExportToExcel("select * from Merafong_sde.sde.URBANRURALCADASTRE where SGTWNEXT = '" & Session("ervenSelected") & "'", Session("ervenSelected"))
    End Sub

    Private Sub btnAgeExport_Click(sender As Object, e As EventArgs) Handles btnAgeExport.Click
        ExportToExcel("select b.* from Properties a inner join Accounts b on a.Sg_No = b.Sg_No where a.Ward = " & dropWardList.SelectedValue, "Ward " & Session("wardSelected"))
    End Sub

    Private Sub btnAgeSearch_Click(sender As Object, e As EventArgs) Handles btnAgeSearch.Click
        Dim query As String
        Dim count As Integer

        ageHeader.Visible = True
        btnAgeExport.Visible = True
        btnAgeZoomto.Visible = True
        count = 0
        query = "select b.* from Properties a inner join Accounts b on a.Sg_No = b.Sg_No where a.Ward = " & dropWardList.SelectedValue
        myConn.Open()
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                count = count + 1
            Loop
        End If
        Session("ageCount") = count
        Session("wardSelected") = dropWardList.SelectedValue
        Session("ageSelected") = dropAgePeroid.SelectedText
        myConn.Close()
    End Sub


    Private Sub searchGrid_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles searchGrid.ItemCommand
        If e.CommandName = "Regulations" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"), "erf")
        ElseIf e.CommandName = "Accounts" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"), "erf")
        ElseIf e.CommandName = "Services" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"), "erf")
        End If
    End Sub

    Private Sub addressGrid_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles addressGrid.ItemCommand
        If e.CommandName = "Regulations" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"), "address")
        ElseIf e.CommandName = "Accounts" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"), "address")
        ElseIf e.CommandName = "Services" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"), "address")
        End If
    End Sub

    Sub generateReport(type As String, sgcode As String, reportType As String)
        Dim query As String
        Session("Sg_No") = sgcode
        myConn.Open()
        query = "select a.Sg_No, a.Stand_No, a.Portion, a.Street_Nam, a.Street_No, b.SGTWNEXT, a.Extension, a.Property_S, a.Purchase_A, a.Property_V, a.Improvemen, a.Deed_No, a.Ward, c.Regulati_1  from Merafong_sde.sde.PROPERTIES a" & _
                    " inner join Merafong_sde.sde.URBANRURALCADASTRE b" & _
                    " on a.Sg_No = b.SGCODE" & _
                    " inner join Merafong_sde.sde.REGULATIONS c" & _
                    " on a.Sg_No = c.Sg_No" & _
                    " where a.Sg_No = '" & Session("Sg_No") & "'"
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                Session("StandNumber") = rdr.Item("Stand_No")
                Session("Portion") = rdr.Item("Portion")
                Session("StreetName") = rdr.Item("Street_Nam")
                Session("StreetNo") = rdr.Item("Street_No")
                Session("TownExt") = rdr.Item("SGTWNEXT")
                Session("Extension") = rdr.Item("Extension")
                Session("PropertySize") = rdr.Item("Property_S")
                Session("PurchaseAmount") = rdr.Item("Purchase_A")
                Session("PropertyValuation") = rdr.Item("Property_V")
                Session("Improvement") = rdr.Item("Improvemen")
                Session("DeedNumber") = rdr.Item("Deed_No")
                Session("Ward") = rdr.Item("Ward")
                Session("Reservoir") = rdr.Item("Regulati_1")
                Exit Do
            Loop
        End If
        myConn.Close()

        If reportType = "address" Then
            Dim i As Integer
            myConn.Open()
            query = "SELECT * FROM [Merafong_SDE].[sde].[REGULATIONS]  where Sg_No = '" & Session("Sg_No") & "'"
            cmd = New SqlCommand(query, myConn)
            rdr = cmd.ExecuteReader()
            i = 1
            If rdr.HasRows Then
                Do While rdr.Read
                    Select Case i
                        Case 1
                            Session("Reservoir") = rdr.Item("Regulati_1")
                        Case 2
                            Session("Reservoir1_2") = rdr.Item("Regulati_1")
                        Case 3
                            Session("Reservoir1_3") = rdr.Item("Regulati_1")
                        Case 4
                            Session("Reservoir1_4") = rdr.Item("Regulati_1")
                        Case 5
                            Session("Reservoir1_5") = rdr.Item("Regulati_1")
                        Case 6
                            Session("Reservoir1_6") = rdr.Item("Regulati_1")
                        Case 7
                            Session("Reservoir1_7") = rdr.Item("Regulati_1")
                    End Select
                    i = i + 1
                Loop
            End If
            myConn.Close()
        End If

        If type = "Accounts" Then
            Dim i As Integer
            myConn.Open()
            query = "SELECT * FROM [ACCOUNTS]  where Sg_No = '" & Session("Sg_No") & "'"
            cmd = New SqlCommand(query, myConn)
            rdr = cmd.ExecuteReader()
            i = 1
            If rdr.HasRows Then
                Do While rdr.Read
                    Session("Balance") = rdr.Item("Balance")
                    Session("AgeCurrent") = rdr.Item("Age_Curren")
                    Session("Age30") = rdr.Item("Age_30")
                    Session("Age60") = rdr.Item("Age_60")
                    Session("Age90") = rdr.Item("Age_90")
                    Session("Age120") = rdr.Item("Age_120")
                Loop
            End If
            myConn.Close()
        End If

        Select Case type
            Case "Regulations"
                If reportType = "address" Then
                    Session("addressSearch") = True
                ElseIf reportType = "erf" Then
                    Session("addressSearch") = False
                End If
                Response.Redirect("RegulationsReport.aspx")
            Case "Accounts"
                Response.Redirect("AccountsReport.aspx")
            Case "Services"
                Response.Redirect("ErfReport.aspx")
        End Select

    End Sub

    Protected Sub ExportToExcel(query As String, tableName As String)

        Dim strConnString As String = ConfigurationManager.ConnectionStrings("Merafong_sde").ConnectionString
        Dim dt As DataTable
        Using con As New SqlConnection(strConnString)
            Using cmd As New SqlCommand(query)
                Using sda As New SqlDataAdapter()
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    'Using dt As New DataTable()
                    dt = New DataTable
                    sda.Fill(dt)
                    dt.TableName = tableName
                    'End Using
                End Using
            End Using
        End Using

        Dim wb As New Excel.XLWorkbook
        wb.Worksheets.Add(dt)
        ' Prepare the response
        Dim httpResponse As HttpResponse = Response
        httpResponse.Clear()
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        httpResponse.AddHeader("content-disposition", "attachment;filename=" & tableName & ".xlsx")

        ' Flush the workbook to the Response.OutputStream
        Using memoryStream As New MemoryStream()
            wb.SaveAs(memoryStream)
            memoryStream.WriteTo(httpResponse.OutputStream)
            memoryStream.Close()
        End Using

        httpResponse.[End]()

    End Sub

 

End Class