﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="Merafong._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron vertical-center">
        <h3 style="text-align:center;">Welcome to the Geographic Information System (GIS) part of the Merafong Intranet.</h3>

        <h5 style="text-align:center;">Here you will find, easy to-use, state of the art, browser-based functionality to view interactive maps of your municipality</h5>
        <p style="text-align:center;">
            <img src="Images/mid_map2.jpg" />
        </p>
    </div>
    

</asp:Content>
