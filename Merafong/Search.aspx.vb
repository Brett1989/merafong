﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports Telerik.Web.UI


Public Class Search
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Merafong_sde").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim page As String

        erf.Visible = False
        street.Visible = False
        count.Visible = False
        age.Visible = False

        page = Request.QueryString("page")
        Select Case page
            Case "erfNumber"
                erf.Visible = True
            Case "streetAddress"
                street.Visible = True
            Case "erven"
                count.Visible = True
            Case "ageAnalysis"
                age.Visible = True
        End Select

    End Sub

    Private Sub btnErfSearch_Click(sender As Object, e As EventArgs) Handles btnErfSearch.Click
        searchGrid.Visible = True
        Session("Stand_No") = txtErf.Text
    End Sub


    Private Sub searchGrid_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles searchGrid.ItemCommand
        If e.CommandName = "Regulations" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"))
        ElseIf e.CommandName = "Accounts" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"))
        ElseIf e.CommandName = "Services" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("Sg_No") = item("Sg_No").Text
            generateReport(e.CommandName, Session("Sg_No"))
        End If
    End Sub

    Sub generateReport(type As String, sgcode As String)
        Dim query As String
        Session("Sg_No") = sgcode
        myConn.Open()
        query = "select a.Sg_No, a.Stand_No, a.Portion, a.Street_Nam, a.Street_No, b.SGTWNEXT, a.Extension, a.Property_S, a.Purchase_A, a.Property_V, a.Improvemen, a.Deed_No, a.Ward, c.Regulati_1  from Merafong_sde.sde.PROPERTIES a" & _
                    " inner join Merafong_sde.sde.URBANRURALCADASTRE b" & _
                    " on a.Sg_No = b.SGCODE" & _
                    " inner join Merafong_sde.sde.REGULATIONS c" & _
                    " on a.Sg_No = c.Sg_No" & _
                    " where a.Sg_No = '" & Session("Sg_No") & "'"
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                Session("StandNumber") = rdr.Item("Stand_No")
                Session("Portion") = rdr.Item("Portion")
                Session("StreetName") = rdr.Item("Street_Nam")
                Session("StreetNo") = rdr.Item("Street_No")
                Session("TownExt") = rdr.Item("SGTWNEXT")
                Session("Extension") = rdr.Item("Extension")
                Session("PropertySize") = rdr.Item("Property_S")
                Session("PurchaseAmount") = rdr.Item("Purchase_A")
                Session("PropertyValuation") = rdr.Item("Property_V")
                Session("Improvement") = rdr.Item("Improvemen")
                Session("DeedNumber") = rdr.Item("Deed_No")
                Session("Ward") = rdr.Item("Ward")
                Session("Reservoir") = rdr.Item("Regulati_1")
            Loop
        End If
        myConn.Close()
        Select Case type
            Case "Regulations"
                Response.Redirect("RegulationsReport.aspx")
            Case "Accounts"
                Response.Redirect("AccountsReport.aspx")
            Case "Services"
                Response.Redirect("ErfReport.aspx")
        End Select

    End Sub
End Class