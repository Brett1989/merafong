﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ErfReport.aspx.vb" Inherits="Merafong.ErfReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
     <style>
        td{
            padding-left: 5px;
        }
        th {
            color: black;
            background-color: gray;
            width: 200px;
            text-align: right;
        } 
        table, tr, th, td{
            border: 1px solid black;
            border-collapse: collapse;
             font-size: medium ;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="jumbotron">
          <h2 style="text-align:center;">Service Erf Report.</h2>
                <table  style="width:100%">
                    <tr>
                        <th>Stand Number:</th>
                        <td><%=Session("StandNumber") %></td>
                        <th>Portion:</th>
                        <td><%=Session("Portion") %></td>
                    </tr>
                    <tr>
                        <th>Street Name:</th>
                        <td><%=Session("StreetName") %></td>
                        <th>Street Number:</th>
                        <td><%=Session("StreetNo") %></td>
                    </tr>
                    <tr>
                        <th>SG Code:</th>
                        <td colspan="3"><%=Session("Sg_No") %></td>
                    </tr>
                     <tr>
                        <th>Town Extension:</th>
                        <td><%=Session("TownExt") %></td>
                        <th>Extention Number:</th>
                        <td><%=Session("Extension") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Property Size:</th>
                        <td colspan="3"><%=Session("PropertySize") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Purchase Amount:</th>
                        <td colspan="3"><%=Session("PurchaseAmount") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Property Valuation:</th>
                        <td colspan="3"><%=Session("PropertyValuation") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Improvement Valuation:</th>
                        <td colspan="3"><%=Session("Improvement") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Deed Number:</th>
                        <td colspan="3"><%=Session("DeedNumber") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Ward Number:</th>
                        <td colspan="3"><%=Session("Ward") %></td>
                    </tr>
                    <tr>
                         <th colspan="4" style="text-align:center">Property Regulations:</th>
                    </tr>
                    <tr>
                        <th colspan="1">Reservoir(CV)(45)</th>
                        <td colspan="3"><%=Session("Reservoir") %></td>
                    </tr>                   
                </table>
            
        </div> 
</asp:Content>
